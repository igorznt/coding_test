from django.test import TestCase
import unittest
import requests
from django.conf import settings
from review import settings as my_settings

settings.configure(default_settings=my_settings, DEBUG=True)

USER_ID = 1 #Replace with the user id that will be tested
TOKEN = "d74259a3d15510a102c1ac1e5a8bc13c159c2eec" #Replace generated token for the user that will be tested
class TestApp(unittest.TestCase):
    review_URL = "http://localhost:8000/api/reviews/"
    # Creating review
    def test_review(self):
        response = requests.post(
                        self.review_URL,
                        headers={'Authorization': 'Token {} '.format(TOKEN)}, 
                        data={ 
                            "rating": 5, 
                            "title": "Best in town", 
                            "summary": "Wow. Impressive", 
                            "company_name": "Dunkin Donalds"})
        self.assertIn(response.status_code, [200, 201])
    
    # Creating review with wrong token
    def test_wrong_token_review(self):
        response = requests.post(
                        self.review_URL,
                        headers={'Authorization': 'Token 3112323 '}, 
                        data={ 
                            "rating": 5, 
                            "title": "Best in town", 
                            "summary": "Wow. Impressive", 
                            "company_name": "Dunkin Donalds"})
        self.assertIn(response.status_code, [400, 401])

    # Creating review with invalid rating
    def test_invalid_rating_review(self):
        response = requests.post(
                        self.review_URL,
                        headers={'Authorization': 'Token {} '.format(TOKEN)}, 
                        data={ 
                            "rating": 7, 
                            "title": "Worst", 
                            "summary": "Not Impressive", 
                            "company_name": "Mc Donuts"})
        self.assertIn(response.status_code, [400, 401])

    # Creating review with invalid data
    def test_invalid_rating_data(self):
        response = requests.post(
                        self.review_URL,
                        headers={'Authorization': 'Token {} '.format(TOKEN)}, 
                        data={ 
                            "rating": 5, 
                            "title": "Best in town", 
                            "summary": "Wow. Impressive"})
        self.assertIn(response.status_code, [400, 401])

    # Retrieving my reviews
    def test_get_reviews(self):
        response = requests.get(
                        self.review_URL,
                        headers={'Authorization': 'Token {} '.format(TOKEN)})
        data = response.json()
        self.assertIn(response.status_code, [200, 201])
        for review in data:
            self.assertEqual(review["user"], USER_ID) 
        


if __name__ == '__main__':
    unittest.main()