from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import status
from rest_framework.views import APIView
from .models import Company, Review
from .serializers import CompanySerializer, ReviewSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.contrib.auth import get_user_model
User = get_user_model()

class ReviewViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        queryset = Review.objects.filter(
            user= User.objects.get(username=self.request.user))
        serializer = ReviewSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        try:
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            if x_forwarded_for:
                ip = x_forwarded_for.split(',')[0]
            else:
                ip = request.META.get('REMOTE_ADDR')
            company = Company.objects.filter(name__iexact=request.data["company_name"])
            if company:
                company = company[0]
            else:
                company = Company(name=request.data["company_name"])
                company.save()
            user = User.objects.get(username=request.user)
            rating = int(request.data["rating"][0])
            if rating < 1 or rating > 5:
                return Response("Invalid data. Rating must be between 1 to 5.", status=status.HTTP_400_BAD_REQUEST)
            review = Review(
                rating=rating,
                title=request.data["title"],
                summary=request.data["summary"],
                ip_address=ip,
                company=company,
                user=user
            )
            review.save()
            return Response("Review created with success.", status=status.HTTP_201_CREATED)
        except:
            import traceback
            traceback.print_exc()
            serializer = CompanySerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response("Invalid data.", status=status.HTTP_400_BAD_REQUEST)