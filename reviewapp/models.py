from django.db import models
from django.contrib.auth import get_user_model
from datetime import datetime
User = get_user_model()

# Create your models here.
class Company(models.Model):
    name = models.CharField(unique=True, blank=False, max_length=200)

class Review(models.Model):
    rating = models.IntegerField()
    title = models.CharField(max_length=64, null=False)
    summary = models.CharField(max_length=10000, null=False)
    ip_address = models.CharField(max_length=20, null=False)
    submission_date = models.DateField(default=datetime.now)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
