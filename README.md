# coding_test  

This project is a coding test Challenge. It is a simple API that allows users to post and retrieve their reviews.    
  
### Requirements: 
```
Python3  
Pipenv 
```

### Before Running
```
pipenv shell  
pipenv install
python manage.py makemigrations #First time  
pytho manage.py migrate  
python manage.py createsuperuser --username=<username> --email=<username>@example.com
``` 

### Running:  
```
pipenv shell
python manage.py runserver  
```
  
### Admin:  
http://localhost:8000/api/admin/ 
  
### Using:  
Creating a review. To create a review for a user you must have a token and create a Post   request:  
```
URL = http://localhost:8000/api/review/  
Header  = {'Authorization': 'Token <TOKEN> '}  
Data = { "rating": 5, "title": "Title Text", "summary": "Summary Text", "company_name": "Company Name"} 
```

### Before Testing:
Create a toke for a user
```
python manage.py drf_create_token <username> #First time or for every user you create
```
 Substitute the TOKEN var on test.py


### Testing:
    python3 test.py  
    